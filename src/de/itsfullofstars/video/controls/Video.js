/* eslint-disable no-eval */
/*
 * Tobias Hofmann
 * Contact: https://www.itsfullofstars.de
 */
sap.ui.define([
	"./../library", 
	"sap/ui/core/Control", 
	"./VideoRenderer"
], function (library, Control, 	VideoRenderer) {
	"use strict";

	/**
	 * Constructor for a new Video control.
	 *
	 * @param {string} [sId] id for the new control, generated automatically if no id is given
	 * @param {object} [mSettings] initial settings for the new control
	 *
	 * @class
	 * Some class description goes here.
	 * @extends sap.ui.core.Control
	 *
	 * @author Tobias Hofmann
	 * @version 1.0.0
	 *
	 * @constructor
	 * @public
	 * @alias de.itsfullofstars.video.controls.Video
	 * @ui5-metamodel This control/element also will be described in the UI5 (legacy) designtime metamodel
	 */
	var Video = Control.extend("de.itsfullofstars.video.controls.Video", {
		metadata: {
			library: "de.itsfullofstars.video",
			properties: {
				/**
				 * Source location of the video file
				 */
				"source": {
					type: "string",
					defaultValue: null
				},
				/**
				 * Type of the video file
				 */
				"type": {
					type: "string",
					defaultValue: "video/mp4"
				},
				/**
				 * Width of the preview window in pixels
				 */
				"width": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "640px"
				},
				/**
				 * Height of the preview window in pixels
				 */
				"height": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "480px"
				},
				/**
				 * Width of the video capture window in pixels
				 */
				"videoWidth": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "1280px"
				},
                /**
                 * Height of the video capture window in pixels
                 */
                "videoHeight": {
					type: "sap.ui.core.CSSSize",
					defaultValue: "960px"
	            },
				/**
				 * Show or hide the HTML5 video controls (play, stop,pase, fullscreen) of the browser
				 */
				"showControls": {
					type: "boolean",
					group: "Behavior",
					defaultValue: true

				},
				/**
				 * Autoplay are video or not
				 */
				"autoplay": {
					type: "boolean",
					group: "Behavior",
					defaultValue: false
				}
			},
			events: {				
			}
		},
		
		renderer: VideoRenderer,
		
		/**
		 * Initialize data
		 */
		init: function () {
        },
        
        
    	/**
		 * Stops the video.
		 * No new images will be captured. Should be called by the UI5 app when the barcode code was successfully read.
		 * @public
		 */
		stopVideo: function(){
			var oVideo = this._getVideo();
			oVideo.stop();					
		},


		/**
		 * Starts playing the video.
		 * @public
		 */
		play: function() {
			var oVideo = this._getVideo();
			oVideo.play();
			console.log(oVideo);
		},
		
		
        /**
         * Sets up video control as given by properties. 
		 * The load command ensures that the video is loaded when the source is changed.
         * @public
         */
        onAfterRendering: function (evt) {
        	try {
				var oVideo = this._getVideo();
				oVideo.controls = this.getShowControls();
				oVideo.autoplay = this.getAutoplay();
				oVideo.load();
        	} catch(err) {}
		},
		

		/**
		 * Called when UI5 is cleaning up resources.
		 * Will end the capturing of images through the camera.
		 * @public
		 */
		onExit: function() {
			this.stopVideo();	
		},
		
		
	    /**
		 * Retrieves the canvas element from the DOM
		 * @private
		 */
    	_getVideo: function() {
    		if (this.getDomRef()) {
        		return this.getDomRef().lastElementChild;
        	} else {
        		return null;
        	}
    	}
		
		
    });
	return Video;
}, /* bExport= */ true);