/*!
 * Tobias Hofmann
 * Contact: https://www.itsfullofstars.de
 */

sap.ui.define([],

	function () {
		"use strict";

		/**
		 * VideoRenderer renderer.
		 * @namespace
		 */
		var VideoRenderer = {
			"apiVersion": 2
		};
		
		/**
		 * Renders the HTML for the given control, using the provided
		 * This will render a HTML like
		 * div
		 * 	video
		 * 		source
		 * {@link sap.ui.core.RenderManager}.
		 *
		 * @param {sap.ui.core.RenderManager} oRm
		 *            the RenderManager that can be used for writing to
		 *            the Render-Output-Buffer
		 * @param {sap.ui.core.Control} oControl
		 *            the control to be rendered
		 */
		VideoRenderer.render = function (oRm, oControl) {
			
			oRm.openStart("div", oControl);
			oRm.class("videoPlayer");
			oRm.style("width", oControl.getWidth());
			oRm.style("height", oControl.getHeight());
			oRm.openEnd();

				oRm.openStart("video");
				oRm.style("width", oControl.getVideoWidth());
				oRm.style("height", oControl.getVideoHeight());
				oRm.attr("preload", "auto");
				oRm.openEnd();

					oRm.text("Your browser does not support the video tag.");

					oRm.openStart("source");
					oRm.attr("src", oControl.getSource());
					oRm.attr("type", oControl.getType());
					oRm.openEnd();
					oRm.close("source");

				oRm.close("video");
			oRm.close("div");

		};

		return VideoRenderer;

	}, /* bExport= */ true);